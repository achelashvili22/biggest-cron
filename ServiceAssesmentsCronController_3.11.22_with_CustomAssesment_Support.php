<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/** Third Party Models */
use App\MORaskSettings;
use App\NumberExcluded;
use App\NumberBlackListed;
use App\NumberAutoDialer;
use App\NumbersVoiceRecords;
use App\ChatWorkingHour;
use App\Company;
use App\RatingPage;
use App\cronLOG;
use App\MorExtension;
use App\IncomingCallsMor;
use App\NumberSmsResponder;

use App\Services\IntelPhoneService;
use Illuminate\Http\Request;

class ServiceAssesmentsCronController extends Controller
{
    /** SASS ( Service Assesments Command - CRON )**/

    /**
     *      each company has new feature: Service Assesments
     *    which means that this Command is able to process Mulitple Data of Phone Calls.
     *    Two Options:
     *    [ Queue Log ] and [ Device IDs ]
     *    1. QUEUE Log takes from database each company Queue Log names and runs MOR Api
     *       To receive MOR response on QUEUE log. This process takes about 1 minutes on each
     *       foreach loop. If we have 4 Logs selected so it will take arount 4 minutes to process
     *       received json data is parsed individually and taken numbers are Compared with
     *       BLACKLIST(for current company ONLY) and EXCLUDE list (for current company ONLY).
     *       grabbs CallID-s and collects them with received phone numbers
     *       so then we have array - [ phone ] => [ voice id ]
     *       this numbers are saved in table for MOR Autodialer (function receives company_id and autodialerID)
     *       AutoDialer MOR function Takes about 3minutes to Take Job & Call. After IVR Poll Data received
     *       Command starts to compare Received Numbers are equal with numbers in Autodialer Table. From then
     *       it saves Poll Data in Database for Each Company
     * 
     *    2. Device ID call Log takes data from MOR Api. it doesn`t take any special time to process data.
     *       Received Json is parsed and Phone numbers are saved in table Autodialer - compared with
     *       BlackList and Exclude List foreach Company. There`s another API function , which receives
     *       From / To Dates & Sourse/Destination Numbers. Some Numbers have multiple VoiceIDs so every of them
     *       is saved in Database for each Company. Same Story happens with this numbers and in 
     *       [ Service Assesments ] - [ Statistic ] We see Currently Authorised Company Votes Received 
     *       after IVR. 
     *       
     */

    /**
     *      
     *      09.14.20
     *      Each Company Has its Own AUTODIALER ID  
     * 
     */

    protected $settings;
    protected $NumberBlackListed;
    protected $cronInterval; 
    protected $intelService;

    public function __construct(IntelPhoneService $iService)
    {
        // get all companies who have Scheduled Settings with Parameters
        $this->settings = MORaskSettings::where('isScheduled',1)->where('autodialer_id','!=',0)->get();
        //Blacklisted Numbers
        $this->NumberBlackListed = NumberBlackListed::all();

        $this->cronInterval      = 15; //15 minutes

        $this->intelService = $iService;
    }

    /**
     *   UPDATE SCHEDULED COMPANY EXTENSIONS 1/24hours
     */
    public function updateExtensions(){

        if(count($this->settings) == 0) dd(">> Not Any Company has Scheduled Settings. ".Carbon::now());

        /** Foreach Company */
        foreach ($this->settings as $key => $setting)
            $data = $this->updateCompanyExtensions($setting->company_id);

        $executeTimeFinish = Carbon::now();
        $executeTimeFinish = $executeTimeFinish->diff(Carbon::now())->format('%I:%S');

        $this->createCronLOG("success",$company_id,"System:Extensions Update FINISH. Average MIN ".$executeTimeFinish);
    }


    /**
     *    SERVICE ASSESMENTS MAIN FUNCTION
     */
    public function index(){

        /** Testing Codes */

        /** To Restart Autodialer */
        //dd($this->restartCampaignStatus());

        /** Retrieve IVR Poll Data Results if Exists */
        // dd($this->curlRetrieveIVRPollData(5,17));
        // dd(1);

        //dd($this->numberIsBlackListed('598618319'));

        // $params = [];
        // $params["company_id"] = 155;
        // $params["setting"] = MORaskSettings::where("id",155)->first();
        // $this->settings = MORaskSettings::all();
        // dd($this->processMORAutoDialer($params));

        

        // $params = [];
        // $params["setting"]=MORaskSettings::where("id",101)->first();
        // $params["company_id"]=155;
        // $params["mor_ip"]="new";
        // $params["admin_company_id"]=70;
        // dd($this->processSmsQueueSend($params));

        // $params = [];
        // $params["campgain_user"]="42230";
        // $params["campgain_id"]=30;
        // $params["mor"]="new";

        // dd($this->intelService->companyAutodialerWakeUp($params));

        /** Refresh BlackList Numbers */
        $this->processBlackListAndSmsResponder();

        /** Don`t Run CRON if Not any Setting is presented */
        if(count($this->settings) < 1){
            $this->createCronLOG("error",-1,"CRON: Company Settings NULL.");
            dd("STOP EXECUTE >>");
        }
            
        /** Loop Through Company Settings */    
        $this->createCronLOG('success',-1,"CRON: Companies COUNT. [".count($this->settings)."]");
        $this->createCronLOG('success',-1,"CRON: START. ".Carbon::now());

        /** Loop Over Scheduled Companies */
        foreach ($this->settings as $key => $setting) {

            /** check if Number Saved in AutoDialer is Either Excluded Or BlackListed */
            $this->refreshAutoDialerData($setting->company_id);
            
            /** TRY
             *  Not Look at Working Hours
             *  Since 11.13.2021
             */
            $this->adjustOptimalTimingOnLastCall($setting);

            /** Detect Each Company who has Saved Settings */
            $currentCompany = Company::where('id',$setting->company_id)->first();

            /** PARAMETERS */
            $params["company_id"]       = $setting->company_id;
            $params["setting"]          = $setting;
            $params["mor_ip"]           = $currentCompany->mor_ip;
            $params['s_user']           = $currentCompany->mordb_user_id;

            /** for MOR Queue Log - without TIMESTAMPS */
            $params["base_from"]             = Carbon::parse($setting->ask_from);
            $params["base_till"]             = Carbon::parse($setting->ask_at);

            $params["base_from_stamp"]       = Carbon::parse($setting->ask_from)->timestamp;
            $params["base_till_stamp"]       = Carbon::parse($setting->ask_at)->timestamp;

            $this->createCronLOG("success",$currentCompany->id,"CRON: ASK Settings UPDATE. [ FROM ".$setting->ask_from." TILL ".$setting->ask_at." ]");
            // 2020-09-24 15:00:00 -> 2020-09-24 17:00:00

            //if mistakenly removed ID
            if(!isset($setting->autodialer_id)){
                /** Save CRON Log */
                $this->createCronLOG("error",$currentCompany->id,"CRON: Company AutoDialer ID NULL.");
                continue;
            }

            //else adjust ID
            $params["autodialer_id"]    = $setting->autodialer_id;

            //Admin Intelphone ID
            $params["admin_company_id"] = $currentCompany->admin_company_id;

            //custom parameters
            $customParams = json_decode($setting->custom_params,TRUE);

            /** 
             *   Custom Assesment Space 
             * 
             *   Since: 2.23.2022
            */

            $needsAutodial=false;
            $needsSmsSend=false;

            dump(">> Autodial Assesment Check");


            /** 
             *  AUTODIALER
             *  @var autodial_queues (array)
             *  @var autodial_extensions (array)
             * 
             *  CDRs
             *  @var autodial_queue_cdr (int)
             *  @var autodial_extensions_cdr (int)
             */


            /** if @var autodial_queues PRESENT */
            if(isset($customParams["autodial_queues"])){

                $params['queueNames'] = $customParams["autodial_queues"];
                $this->createCronLOG("success",$currentCompany->id,"CRON: Autodialer [QUEUE] Log START.");

                $this->processMORQueueLog($params);
                $needsAutodial=true;
            }

            /** if @var autodial_extensions PRESENT */
            if(isset($customParams["autodial_extensions"])){
                $params['extensions'] = $customParams["autodial_extensions"];

                $this->createCronLOG("success",$currentCompany->id,"CRON: Autodialer [DEVICE] Log START.");
                $this->processMORDeviceCallLog($params);
                $needsAutodial=true;
            }

            /** if @var autodial_queue_cdr PRESENT */
            if(isset($customParams["autodial_queue_cdr"]) && $customParams["autodial_queue_cdr"] == 1){

                $this->createCronLOG("success",$currentCompany->id,"CRON: Autodialer [QUEUE CDR] Log START.");
                $this->getCompanyCDR($params,TRUE); //QUEUEs CDR
                $needsAutodial=true;
            }

            /** if @var autodial_extensions_cdr PRESENT */
            if(isset($customParams["autodial_extensions_cdr"]) && $customParams["autodial_extensions_cdr"] == 1){

                $this->createCronLOG("success",$currentCompany->id,"CRON: Autodialer [DEVICE CDR] Log START.");
                $this->getCompanyCDR($params,FALSE); //Extensions CDR
                $needsAutodial=true;
            }



            $this->createCronLOG("success",$currentCompany->id,$needsAutodial==true?"CRON: Company Autodialer SASS CAPABLE.":"CRON: Company Autodialer SASS NOT CAPABLE.");

            /** Process MOR Autodialer if any AUTODIALER KEY presented */
            if($needsAutodial==true)
                $data = $this->processMORAutoDialer($params);



            /* =================================================================================== */

            dump(">> SMS Assesment Check");


            /** 
             *  SMS Assesment
             *  @var sms_queues (array)
             *  @var sms_extensions (array)
             * 
             *  CDRs
             *  @var sms_queue_cdr (int)
             *  @var sms_extensions_cdr (int)
             */



            /** if @var sms_queues PRESENT */
            if(isset($customParams["sms_queues"])){

                $params['queueNames'] = $customParams["sms_queues"];
                $this->createCronLOG("success",$currentCompany->id,"CRON: Sms Assesment [QUEUE] Log START.");

                $this->processMORQueueLog($params);
                $needsSmsSend=true;
            }

            /** if @var sms_extensions PRESENT */
            if(isset($customParams["sms_extensions"])){

                $params['extensions'] = $customParams["sms_extensions"];
                $this->createCronLOG("success",$currentCompany->id,"CRON: Sms Assesment [DEVICE] Log START.");
                
                $this->processMORDeviceCallLog($params);
                $needsSmsSend=true;
            }

            /** if @var sms_queue_cdr PRESENT */
            if(isset($customParams["sms_queue_cdr"]) && $customParams["sms_queue_cdr"] == 1){

                $this->createCronLOG("success",$currentCompany->id,"CRON: Sms Assesment [QUEUE CDR] Log START.");
                $this->getCompanyCDR($params,TRUE); //QUEUEs CDR
                $needsSmsSend=true;
            }

            /** if @var sms_extensions_cdr PRESENT */
            if(isset($customParams["sms_extensions_cdr"]) && $customParams["sms_extensions_cdr"] == 1){

                $this->createCronLOG("success",$currentCompany->id,"CRON: Sms Assesment [DEVICE CDR] Log START.");
                $this->getCompanyCDR($params,FALSE); //Extensions CDR
                $needsSmsSend=true;
            }



            $this->createCronLOG("success",$currentCompany->id,$needsSmsSend==true?"CRON: Company SMS SASS CAPABLE.":"CRON: Company SMS SASS NOT CAPABLE.");

            /** Process MOR Sms Assesment if any SMS ASSESMENT KEY presented */
            if($needsSmsSend==true)
                $data = $this->processSmsQueueSend($params);

            /** Custom Assesment Space END /.. */

            /** Small Delay for Each Company */
            $this->createCronLOG("success",$currentCompany->id,"CRON: Company FINISH. Delay 10 SEC.");
            sleep(10);
        /** Loop End */
        }

        $executeTimeFinish = Carbon::now();
        $executeTimeFinish = $executeTimeFinish->diff(Carbon::now())->format('%I:%S');
        
        $this->createCronLOG("success",$currentCompany->id,"CRON: FINISH ".Carbon::now()." Company COUNT ".count($this->settings)." Time AVG ".$executeTimeFinish);
    
        dd("CRON FINISH. ".Carbon::now());
    }/** Index .END */


    /**
     *   Create CronLOG
     *   @var namespace - namespace of controller
     *   @var company - company ID or -1 for SYSTEM //11.12.2021
     *   @var log      - desired Error
     */
    protected function createCronLOG($namespace,$id,$log){

        $vendor = $id==-1?"SASS":($id==-2?"IVR":$id); 

        /** Save CRON Log */
        cronLOG::create([
            'namespace' => $namespace,
            'vendor'=> $vendor,
            'log' => $log
        ]);

        return "success";
    }

    /**
     *     MOR Device Outgoing Call LOG Processor
     * 
     *     @var array extensions - [extension Description]
     *     @var params - parameters with dates->timestamp;
     */
    protected function processMORDeviceCallLog($params){
     
        /** Final Working Approach 11.10.20 */

        /**
         *     
         *    Retrieving Device Extensions from $params
         *    Parseing Extensions with Callerids
         *    Example: "296" <322492020>
         *    Should Be only <number> in callerid FIELD!!
         */
        $extensions = $params["extensions"];

        foreach ($extensions as $key => $extension) {
            //find Extension
            $morExtension = DB::table('mor_extension')->where('extension',$extension)->where('company_id',$params['company_id'])->get();

            //result:: "296" <322492020>
            if(isset($morExtension)){
                // 9.15.20 callerIDs still have dirty data ex: "322492020" <322492020>
                // force Remove Everything till "<"
                $callerID = substr($morExtension->first()->callerid,strpos($morExtension->first()->callerid,"<"));
                $extensions[$key] = '"'.$extensions[$key].'" '.$callerID;
            }
        }

        /**
         *      EXAMPLE
         *    [ "296" <322492020> ] = [ ["number1"] , ["number2"] ...]
         * 
         */
        $numExtensDest = [];

        $queryParams = [];
        $queryParams['period_start'] = $params["base_from_stamp"];
        $queryParams['period_end'] = $params["base_till_stamp"];

        $company = DB::table('companies')
            ->where('id', $params['company_id'])
            ->first();

        // MOR user
        $queryParams["s_user"]      = $company->mordb_user_id;
        // all Answered Calls
        $queryParams["s_call_type"] = 'answered';
        // all devices
        $queryParams["s_device"]    = 1;

        $hash                       = sha1(implode("",array_values($queryParams))."case123");
        $queryParams["u"]           = "admin";
        $queryParams["hash"]        = $hash;
        
        $url                        = config('mor.user_calls_get.'.$company->mor_ip)."?".http_build_query($queryParams);

        /** Process Curl Post */
        $result = $this->curlPost($url, [
            'u'     => $queryParams["u"],
            'hash'  => $queryParams["hash"],
        ]);

        $xml2json   = json_encode(simplexml_load_string($result), true); // convert xml data to json
        $data       = json_decode($xml2json, true)['calls_stat'];

        // false if no Call Data Received
        if(!isset($data['calls']['call'])){
            $this->createCronLOG("error",$params['company_id'],"DeviceLOG: Call Data EMPTY.");
            return false;   
        } 

        //received data from Devices
        $deviceData = array($data['calls']['call']);

        foreach ($deviceData as $key => $call) {
            /** if 1 or more Call has Done By Extension **/
            if(array_key_exists(0,$call)){
                foreach ($call as $key => $item) {
                    //adjust number
                    $item['dst'] = $this->numberRemoveGEOIndexPlus($item['dst']);

                    //Don`t insert numbers if They are Excluded or BlackListed
                    if($this->isNumberExcluded($item['dst'],$params['company_id']) || $this->isNumberBlackListed($item['dst'],$params['company_id']) || $item['dst'] == "anonymous"){}
                    else // catch ONLY CallerIDs which are saved in $params
                        if(in_array($item['clid'],$extensions)){
                            // [ Local Device Number ] => [0 => number1 ] , [1 => number2 ]
                            $numExtensDest[$item['clid']][] = $item['dst'];
                        }

                }
            }else{
                //remove Prefix 995 and +
                $call['dst'] = $this->numberRemoveGEOIndexPlus($call['dst']); 

                //Don`t insert numbers if They are Excluded or BlackListed
                if($this->isNumberExcluded($call['dst'],$params['company_id']) || $this->isNumberBlackListed($call['dst'],$params['company_id'])){}
                else 
                    if(in_array($call['clid'],$extensions)){
                        // [ Local Device Number ] => [0 => number1 ] , [1 => number2 ]
                        $numExtensDest[$call['clid']][] = $call['dst'];
                    }

            }
        }//array($data['calls']['call'] ../end

        /** Unique Numbers Called AT */
        foreach ($numExtensDest as $key => $value) {
            $numExtensDest[$key] = array_unique($numExtensDest[$key]);
        }

        /** 
         *      TODO::10.15.20 
         *  
         *      Outgoings calls now to be inserted In AutoDialer Table
         * 
         * */ 
        foreach ($numExtensDest as $key => $numberList) {

            // Grab Extension <local number> 
            $ext = each($numExtensDest)[0];
            // remove Local Number
            $ext = substr($ext,1,strrpos($ext,'"')-1);

            foreach ($numberList as $key => $number) {
                NumberAutoDialer::firstOrCreate([
                    'number'     => $this->numberRemoveGEOIndexPlus($number),
                    'company_id' => $params['company_id'],
                    'agent'      => $ext,
                    'has_sms'=> $params["setting"]["has_sms"] == 1 ? 1 : 0
                ]);
            }
        }

        // receive and Parse Voice Recordings
        $this->processVoiceRecords($numExtensDest,$params['company_id']);
    }

    /**
     *     MOR QUEUE LOG Processor
     *     
     *     @var params - [from,till,queueArray]
     */
    protected function processMORQueueLog($params){ //$queueArray,$from,$till


        /** Example Entry
         * 
         *    0 => array:11 [▼
         *      "id" => "79001046"
         *      "time" => "2021-11-19 12:15:07"
         *      "call_id" => "1637309490.28715615"
         *      "queue_name" => "queue_INEX Group Tbilisi Queue"
         *      "agent" => "Local/346@pool_56_mor_local"
         *      "event" => "COMPLETEAGENT"
         *      "data1" => "6"
         *      "data2" => "172"
         *       "data3" => "1"
         *       "data4" => []
         *       "data5" => []
         *   ]
         * 
         */


        //Approach V3 (-15) Minutes 11.26.2021
        $from       = $params["base_from"]->toDateTimeString();
        $till       = $params["base_till"]->toDateTimeString();

        // entries
        $entries    = [];

        $company = DB::table('companies')
            ->where('id', $params['company_id'])
            ->first();


        /** Collecting Entries from DB - since 11.19.2021 */

        /**
         *   [ call_id ] => [ 
         * 
         *      IncomingCallsMor[ENTERQUEUE]
         *      IncomingCallsMor[CONNECT]    
         *      ...
         *  ]
         */

        $entries = IncomingCallsMor::where("time",">=",$from)
                                ->where("time","<=",$till)
                                ->whereIn("queue_name",$params["queueNames"])
                                ->where(function($q){
                                    $q->where("event","ENTERQUEUE")
                                      ->orWhere("event","CONNECT");
                                })->get()->groupBy("call_id");

        //needsSmsEvaluation
        $has_sms = json_decode($params['setting'],TRUE)["has_sms"]==1?1:0;

        //Numbers Count
        $bCount = NumberAutoDialer::where('company_id',$params['company_id'])
                                ->where('has_sms',$has_sms)
                                ->where('sent',0)
                                ->count();

        /** Pass Numbers To AutoDialer & Save Voice Records */                        
        foreach ($entries as $call_id => $queueData){

            $number = null;
            $call_date = null;
            $answer_date = null;
            $voice_ids = null;
            $queueName = null;
            $agent = "";

            if(count($queueData)==1)
                continue;
            

            foreach ($queueData as $id => $item) {

                if($item["event"]=="ENTERQUEUE"){
                    $number = $item["data2"];
                    $call_date = $item["time"];
                }

                if($item["event"]=="CONNECT"){

                    $answer_date = $item["time"];

                    $voice_ids.= ",".$item["data4"];

                    //EXAMPLE: Local/354@pool_56_mor_local
                    $dashPos = strpos($item["agent"],"/")+1;
                    $atPos = strpos($item["agent"],"@");
                    
                    $agent = substr($item["agent"],$dashPos,$atPos-$dashPos);

                    $queueName = $item["queue_name"];
                }

                
                      
            }     

            //if Both Number and VoiceID exists and not equals to [anonymous] - since 11.29.2021
            if(!is_null($number) && !is_null($voice_ids) && $number != "anonymous"){

                //removing +995
                $number = $this->numberRemoveGEOIndexPlus($number);

                //don`t send if Exluded or BlackListed
                if($this->isNumberExcluded($number,$params['company_id']) || $this->isNumberBlackListed($number,$params['company_id'])){}
                else{

                    /** Existance Variable BUILDERS */
                    $numberExistsBuild = NumberAutoDialer::where('number',$number)->where('company_id',$params['company_id']);
                    $VoiceExistsBuild = NumbersVoiceRecords::where('number',$number)->where('company_id',$params['company_id']);

                    if(!$numberExistsBuild->exists())
                        NumberAutoDialer::create([
                            'number'        => $number,
                            'company_id'    => $params['company_id'],
                            'has_sms'       => $has_sms,
                            'sent'          =>0
                        ]);

                    //update voice_records if exists
                    if($VoiceExistsBuild->exists()){
                        
                        $voiceRec = $VoiceExistsBuild->first();
                        // Easy Method 11.30.20.21
                        // $raw = 'AAA,BBB,aAA,BbB,AAA,BbB';
                        // $string = implode(',', array_unique(explode(',', $raw)));

                        $newVoice = $voiceRec["voiceID"].$voice_ids;
                        $unique = implode(',', array_unique(explode(',', $newVoice)));

                        $voiceRec->update(["voiceID"=>$unique]);
                    }
                    else
                        NumbersVoiceRecords::firstOrCreate([
                            'company_id'    => $params['company_id'],
                            'number'        => $number,
                            'has_sms'       => $has_sms, //identify Voice Records which Evaluated by SMS
                            'agent'         => $agent,
                            'voiceID'       => $voice_ids!=""?substr($voice_ids,1):null,
                            'call_date'     =>$call_date,
                            'queue'         => $queueName,
                            'answer_date'   =>$answer_date
                        ]);

                }
                //exluded or blacklisted END.
            }
        }    

        $aCount = NumberAutoDialer::where('company_id',$params['company_id'])
                                        ->where('has_sms',$has_sms)
                                        ->where('sent',0)
                                        ->count();

        $this->createCronLOG("success",$params['company_id'],"QueueLOG: Total Num IMPORT. [".($aCount-$bCount)."]");

        return true;
    }


    /**
     * 
     *      Protected Processor for Autodialer
     *      @var params
     */
    protected function processMORAutoDialer($params){

        $companyID = $params["company_id"];
        $setting = $params["setting"];

        $company = Company::where("id",$companyID)->first();

        if($company["mordb_user"]=="" || is_null($company["mordb_user"])){
            $this->createCronLOG("error",$companyID,"AutoDIALER: No CAMPGAIN User.");
            return false;
        }

        //since 11.19.2021
        $campgainUser = $company["mordb_user"];

        /** Get Current Company Autodialer ID */
        $autoDialerUniqID = $this->settings->where('company_id',$companyID)->first()->autodialer_id;

        /** wakeup MOR Autodialer Campgain if it`s OFF */
        $auParams=[];
        $auParams["campgain_id"]=$autoDialerUniqID;
        $auParams["mor"]=$company->mor_ip;

        $auRes = $this->intelService->companyAutodialerWakeUp($auParams);
        $this->createCronLOG($auRes["status"]=="success"?"success":"error",$companyID,"AutoDIALER: MOR respone: ".json_encode($auRes["message"],TRUE));
        $auParams=$auRes=null;

        /** numbers ready for autodialer For current company */
        $numbers2Dial = NumberAutoDialer::where('company_id',$companyID)
                                        ->where("has_sms",0)
                                        ->where("sent",0)
                                        ->get();

        dump(">> AutoDialer Numbers: ".$numbers2Dial);

        if(empty($numbers2Dial)){
            $this->createCronLOG("error",$companyID,"AutoDIALER: No NUMBERS.");
            return false;
        }
        
        // remove PLUS and Index for each Number in Array
        $numbers2Dial->each(function ($item) {
            $item->update(['number'=>$this->numberRemoveGEOIndexPlus($item->number)]);
        });

        //path to csv file
        $csvPath = Storage::disk('autodialer')->path('autodialer_numbers.csv');

        // create a file pointer connected to the output stream
        $output = fopen($csvPath, 'w');

        // add data to .csv
        $numbers2Dial->each(function ($item) use ($output){
            fputcsv($output, array($item->number));
        });
    
        /** Send Numbers To Current Company`s AutoDialer */
        $dialer = $this->curlAutoDialer($csvPath,$autoDialerUniqID,$campgainUser,$company->mor_ip);
  
            if($dialer == "File is empty"){
                $this->createCronLOG("error",$companyID,"AutoDIALER: CSV EMPTY.");
                return false;
            }

            if($dialer == "Campaign was not found"){
                $this->createCronLOG("error",$companyID,"AutoDIALER: Campgain ID MISTMATCH.");
                return false;
            }
   
        $this->createCronLOG("success",$companyID,"AutoDIALER: Numbers SENT. [".count($numbers2Dial)."]");

        dump(">> AutoDialer Numbers SENT.");

        // Empty file
        ftruncate($output, 0);

        $blackInterval = $params["setting"]["blacklist_int"];

        // update each as SENT to AUTODIALER
        // blacklist each
        $numbers2Dial->each(function ($item) use($companyID,$blackInterval){
            $item->update(['sent'=>1]);

            numberBlackListed::firstOrCreate([
                'number'      => $this->numberRemoveGEOIndexPlus($item->number),
                'company_id'  => $companyID,
                'finishes_at' => Carbon::now()->addHours($blackInterval)
            ]);
        });

        return true;
    }

    /**
     * 
     *   Protected Func Curl Get Voice Records
     *   @var array array - numbers assotiative array [ from ] => [0 => to1], [1 => to2]...
     *   @var company_id - company ID
     */
    protected function processVoiceRecords($array,$company_id){

        $this->createCronLOG("success",$company_id,"VoiceRecords: Voice Recordings GET.");

        /** Process Voice Records From Outgoing */
        foreach ($array as $extension => $numbers) {
            foreach ($array[$extension] as $id => $destination) {
            /** remove Extension from numbers 
             * Example: "296" <322839982> -> [ 322839982 ] */
            if(strpos($extension,'<'))
                $extension = substr($extension,strpos($extension,'<')+1,-1);
                /** Get Voice Record From - Destination */
                $this->curlVoiceRecordsGet($extension,$destination,$company_id);
            }
        }
    }
    

    /** Process BlackList on Each MOR run
     *  Process Number Sms Responder Table
     */
    protected function processBlackListAndSmsResponder(){

        $now = Carbon::now();

        numberBlackListed::each(function($blackNumber) use($now){
            if($now->greaterThanOrEqualTo($blackNumber->finishes_at))
                $blackNumber->delete();
        });

        NumberSmsResponder::each(function($respondNumber) use($now){
            // example: 11.30.2021
            // should be removed: 11.29.2021 / 11.28.2021 ect.
            $createdAt = Carbon::parse(Carbon::parse($respondNumber->created_at)->format("y-m-d"));
            $nowDate = Carbon::parse(Carbon::parse($now)->format("y-m-d"));
            
            if($nowDate->greaterThan($createdAt))
                $respondNumber->delete();
        });
    }

    /** 
     *   Check Numbers Ready for Autodialer
     *   there should not be any number Saved in Autodialer on ever Next ASK
     *   @var company_id - Company ID
    */
    protected function refreshAutoDialerData($company_id){
        $data = NumberAutoDialer::where('company_id',$company_id)->get();
        $data->each(function($num4autodial) use($company_id){
            //if number is Excluded Or BlackListed - remove From AutoDialer
            if($this->isNumberExcluded($num4autodial->number,$company_id) || $this->isNumberBlackListed($num4autodial->number,$company_id))
            $num4autodial->delete();
        });
    }

    /** Remove Country INDEX from number */
    protected function numberRemoveGEOIndexPlus($number){
        //remove PLUS from Number
        if(substr($number,0,1) == "+")
            $number = substr($number,1);
        //remove 995 from Number
        if(substr($number,0,3) == "995")
            $number = substr($number,3);
        return $number;
    }

    /** if Number Excluded 
     *  @var number - desired number
     *  @var companyID - company ID
     *  @return TRUE if exists
    */
    protected function isNumberExcluded($number,$companyID){
        return NumberExcluded::where([['number',$number],['company_id',$companyID]])->exists();
    }

    /** if Number BlackListed 
     *  @var number - desired number
     *  @var companyID - company ID
     *  @return TRUE if exists
    */
    protected function isNumberBlackListed($number,$companyID){
        return NumberBlackListed::where([['number',$number],['company_id',$companyID]])->exists();
    }

    /**
     * 
     *      UPDATE company Extensions
     *      @var company_id - coompany ID
     *      each ASK took average 1 Minutes for 1 companies
     */
    protected function updateCompanyExtensions($company_id){

        if(!Company::where('id',$company_id)->exists()) return false;

        $company = Company::where('id',$company_id)->first();
        // MOR DB Device Get all
        $user_id    = $company->mordb_user_id;
        $secretKey  = config('mor.query.secret');
        $hash       = sha1($user_id.$secretKey);
        
        //where to save DATA
        $data = [];

        $device_url      = config('mor.devices_get.'.$company->mor_ip)."?u=admin&user_id=$user_id&hash=$hash";
        $device_xml2json = json_encode(simplexml_load_string(file_get_contents($device_url)), false);
        $device_loadJson = json_decode($device_xml2json, true);
            
        //receive Data From MOR
        collect($device_loadJson)
                       ->flatten(2)
                       ->pluck('device_id')
                       ->each(function ($key) use(&$data, $secretKey) {
                           $hash       = sha1($key.$secretKey);

                           $device_details_url      = config('mor.device_details_get.'.$company->mor_ip)."?u=admin&device_id=$key&hash=$hash";
                           $device_details_xml2json = json_encode(simplexml_load_string(file_get_contents($device_details_url)), false);
                           $device_details_loadJson = json_decode($device_details_xml2json, true);

                           $device_details  = collect($device_details_loadJson)
                       ->only('description', 'extension', 'callerid');
                       $data[$key]['device']         = $key;
                                                       /** TODO::09.15.20 till now Eloquent was not saving as Array in DB */
                       $data[$key]['description']    = ($device_details['description'] == []) ? "" : $device_details['description'];
                       $data[$key]['extension']      = ($device_details['extension'] == []) ? "" : $device_details['extension'];
                       $data[$key]['callerid']       = ($device_details['callerid'] == []) ? "" : $device_details['callerid'];
        });

        //clear Existed extensions
        $extensions = MorExtension::where("company_id",auth()->user()->company_id)->delete();

        /** Save in DB */
        collect($data)->each(function ($value) {
            MorExtension::create(
                [
                    'device'         => $value['device'],
                    'description'    => $value['description'],
                    'extension'      => $value['extension'],
                    'callerid'       => $value['callerid'],
                    'company_id'     => auth()->user()->company_id,
                ]
            );
        });
       
       // update Last Updated Field for Current Company
       DB::table('companies')
           ->where('id', '=', $company_id)
           ->update([
               'mor_extension_click_data' => date('Y-m-d H:i:s')
       ]);

       //Log success
       $this->createCronLOG("success",$company_id,"Extensions: List UPDATE.");
   }

    /**
     * 
     *   Protected Func Curl Send File
     * 
     *   @var file csvPath -  path of .csv file
     *   @var autodialerID  -  Unique ID of Autodialer
     *   @var campgainUser  -  Unique campgainUser ID
     *   @var mor_ip - Company MOR IP
     * 
     */
    protected function curlAutoDialer($csvPath,$dialerID,$campgainUser,$mor_ip){
        // pass credentials
        $campgain_user     = $campgainUser; //since 11.19.2021
        $secretKey         = 'param123';
        // dialer id
        $campgain_id       = $dialerID;
        $hash       = sha1($campgain_id.$secretKey);

        $url        = config('mor.autodialer_upload_numbers.'.$mor_ip)."?u={$campgain_user}&id={$campgain_id}&hash={$hash}";

        $curlFile = curl_file_create($csvPath);
        $post = array('u' => $campgain_user,'id' => $campgain_id,'file'=> $curlFile );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FAILONERROR, true); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result=curl_exec($ch);
        curl_close ($ch);

        $result   = json_encode(simplexml_load_string($result));
        $result   = json_decode($result, true);
        $result   = $result["status"];
        
        if(isset($result["error"])) return $result["error"];
            else
        return $result["success"];
    }

    /**
     * 
     *      Retrieve IVR Poll Data
     *      @var company_id
     *      @var params
     */
    protected function curlRetrieveIVRPollData($company_id,$params){

        // pass credentials
        $campgain_user     = $params["campgain_user"];
        $secretKey         = 'param456';
        // autodialer INEX id
        $campgain_id       = $params["autodialer_id"];
        $hash       = sha1($campgain_id.$secretKey);
        $url        = config('mor.autodialer_get_campaign_ivr_poll_results.'.$params["mor_ip"])."?u={$campgain_user}&id={$campgain_id}&hash={$hash}";

        $post = array('u' => $campgain_user,'id' => $campgain_id);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FAILONERROR, true); 

        //process Curl
        $url        = str_replace(" ", "%20", $url);
        $xml2json   = json_encode(simplexml_load_string(file_get_contents($url)), true);
        $loadJson   = json_decode($xml2json, true);

        // if ivr_poll_results_string is empty
        if(!isset($loadJson["status"]["ivr_poll_results_string"])){
            $this->createCronLOG("error",$company_id,"IVR: Poll Results EMPTY.");
            //skip company
            return false;
        }

        $ratings = $loadJson["status"]["ivr_poll_results_string"];

        $this->createCronLOG("success",$company_id,"IVR: Votes Parse START.");

        $ratingArray = explode("\n",$ratings);

        $ratingsNew = [];

        // number => vote & date
        $votes = [];
        
        //optimizing Array
        //removing numbers not Having Evaluation
        foreach ($ratingArray as $key => &$item) {
            $item = explode(",",$item);
            if(count($item)==5)
                $ratingsNew[] = $item;
        }

        // fix & issue
        $ratingArray = $ratingsNew;
        
        foreach ($ratingArray as $key => $item) {

            /** New Method
             *  Exploding Comma
             *  Since 11.17.2021
             * 
                0 => "598601361"             // number
                1 => "2021-11-15 16:05:01"   // execute time
                2 => "2021-11-15 16:05:11"   
                3 => "2021-11-15 16:05:15"  
                4 => "4"                     // rating
             *  
             * 
             */
            if(!isset($item[0]))
                continue;

            $phoneNumber = $this->numberRemoveGEOIndexPlus($item[0]);

            if(!isset($item[1]) && !isset($item[2]) && !isset($item[3])) 
                continue;
            
            $date = $item[1];

            if(!isset($item[4])) 
                $voteNum = 0;  
            else
                $voteNum = (int) filter_var($item[4], FILTER_SANITIZE_NUMBER_INT);

            /**
             *   Parsing Votes like
             *   
             *     0 => array:3 [▼
             *           "number" => "598618319"
             *           "date" => "2021-11-17 11:45:01"
             *           "vote" => 5
             *       ]
             */

            $vote = [];
            $vote["number"] = $phoneNumber;
            $vote["date"] = $date;
            $vote["vote"] = $voteNum;
            $votes[] = $vote;

            // number => [ vote ] & [ date ]
            // $votes[$phoneNumber]['vote'] = $voteNum;
            // $votes[$phoneNumber]['date'] = $date;

            /** OLD 
             *  retrieving String FROM
             *  598618319,2020-09-07 14:45:01,2020-09-07 14:45:16,2020-09-07 14:45:21,"4" 
             * 
             */
        }

        /**
         *  Newest
         *  11.18.2021
         */
        $this->createCronLOG("success",$company_id,"IVR: Number Votes SAVE.");
        foreach ($votes as $idx => $vote) {

            /** CDR Check 
             *  if Queue CDR Turned ON
            */

            
            //BUILDER
            $voiceRecords = NumbersVoiceRecords::where('company_id',$company_id)
                ->where('number',$vote["number"])
                ->where('has_sms',0); //don`t evaluate SMS

            // if(!is_null($voiceRecords->first())){

                $rating = RatingPage::where("company_id",$company_id)
                    ->where("number",$vote["number"])
                    ->where("rate_date",$vote["date"])
                    ->where("rate",$vote["vote"])
                    ->exists();

                if(!$rating){

                    RatingPage::create([
                        'number'        => $vote["number"],
                        'company_id'    => $company_id,
                        'rate'          => $vote["vote"],
                        'rate_date'     => $vote["date"],
                        'queue'         => isset($vote["queue"])?$vote["queue"]:null,
                        'agent'         => !is_null($voiceRecords->first()["agent"]) ? $voiceRecords->first()["agent"] : null,
                        'voice_records' => json_encode(explode(",",$voiceRecords->first()["voiceID"]),true)
                    ]);     
                    
                    //clearing Autodialer Number
                    NumberAutoDialer::where('company_id',$company_id)
                                    ->where("number",$vote["number"])
                                    ->where("has_sms",0)
                                    ->where("sent",1)
                                    ->delete();
                }
                //remove Voice IDs for this number
                if(!is_null($voiceRecords->first()))
                    $voiceRecords->each(function($voice){
                        $voice->delete();
                    });
            //}
            
        }
        
        $this->createCronLOG("success",$company_id,"IVR: Data FINISH.");

        return true;
    }


    /** cURL get voice recordings and save them 
     * 
     *  @var source - number called from
     *  @var destination - number called at
     *  @var company_id
    */
    protected function curlVoiceRecordsGet($source,$destination,$company_id){

        $numbersWithVoiceIDs = [];
        // current company settings
        $companySettings = MORaskSettings::where('company_id',$company_id)->first();
        // company MOR DB User
        $company = DB::table('companies')
            ->where('id', $company_id)
            ->first();
        $user = isset($company->mordb_user_id) ? $company->mordb_user_id : -1;

        $fdate = Carbon::parse($companySettings->ask_from)->timestamp;
        $sdate = Carbon::parse($companySettings->ask_at)->timestamp;

        $baseUrl    = config('mor.baseURL.'.$company->mor_ip);
        $username   = config('mor.query.u');
        $secretKey  = config('mor.query.secret');
        $hash       = sha1($secretKey);

        $post = [
            'u' => $username,
            'hash' => $hash,
            'date_from' => $fdate,
            'date_till' => $sdate,
            'destination' => null,
            'source' => null,
            'user' => $user
        ];

        $ch = curl_init($baseUrl.'recordings_get');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $res = curl_exec($ch);

        curl_close($ch);

        $xml = simplexml_load_string($res,'SimpleXMLElement',LIBXML_NOCDATA);
        $json = json_encode($xml);
        $response = json_decode($json, true);

        //skip if No Record & skip if No status recordings received
        if(isset($response['status']['error'])) return false;
        if(!isset($response['status']['recordings']['recording'])) return false;
        
        // voice recordings
        $recordings = array($response['status']['recordings']['recording']);


        foreach ($recordings as $item) {
            //if number has More than 1 Voice Recordings Loop Each
            if(array_key_exists(0,$item)){
                foreach ($item as $voice) {

                    //arrays appear from MOR Since 11.24.2021
                    if(is_array($voice["destination"]) || is_array($voice["source"]))
                        continue;

                    if(strpos($voice["destination"], $destination) !== false && strpos($voice["source"], $source) !== false){
        
                        //grab voice Record ID & remove URL and MP3 ex [ 1425634.34454541123 ]
                        $voiceRecordID = substr($voice["mp3_url"],strpos($voice["mp3_url"],"gs/")+3,-4);

                        /** Save Unique Voice Record ID */
                        if(!NumbersVoiceRecords::where('voiceID',$voiceRecordID)->exists())
                            NumbersVoiceRecords::Create([
                                'company_id'    => $company_id,
                                'number'        => $this->numberRemoveGEOIndexPlus($voice["destination"]),
                                'voiceID'       => $voiceRecordID
                            ]);
                    }
                }
            }//ELSE
            else{

                if(strpos($item["destination"], $destination) !== false && strpos($item["source"], $source) !== false){

                    //grab voice Record ID & remove URL and MP3 ex [ 1425634.34454541123 ]
                    $voiceRecordID = substr($item["mp3_url"],strpos($item["mp3_url"],"gs/")+3,-4);

                    if(!NumbersVoiceRecords::where('voiceID',$voiceRecordID)->exists())
                        NumbersVoiceRecords::Create([
                            'company_id'    => $company_id,
                            'number'        => $this->numberRemoveGEOIndexPlus($item["destination"]),
                            'voiceID'       => $voiceRecordID
                        ]);
                }
            }
        }
    }

    /** 
     * 
     *    Protected Func Curl INIT 
     *   
     *    cURL Post By @Zviad
     * 
     * */
    protected function curlPost($url, $data=NULL, $headers = NULL)
    {
        $ch = curl_init($url);
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if(!empty($data)){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            trigger_error('Curl Error:' . curl_error($ch));
        }

        curl_close($ch);

        return $response;
    }

    /** Optimal
     *  Adjust ASK_FROM ASK_AT
     *  since 11.14.2021
     *  @var setting
     */
    protected function adjustOptimalTimingOnLastCall($setting){

        /** Last Optimized at 11.26.2021 */

        $minute = (int)Carbon::now()->second(0)->format("i");

        if($minute%$this->cronInterval==0)
            $setting->update([
                "ask_from"=>Carbon::now()->subMinutes(2*$this->cronInterval)->second(0),
                "ask_at"=>Carbon::now()->subMinutes($this->cronInterval)->second(0),
            ]);
        else 
            $setting->update([
                "ask_from"=>Carbon::now()->subMinutes(2*($minute+$this->cronInterval))->second(0),
                "ask_at"=>Carbon::now()->subMinutes($minute+$this->cronInterval)->second(0),
            ]);


        return true;        
    
        // $minute = Carbon::now()->second(0)->format("i");

        // if(is_null($setting->last_cron_at))
        //     if($minute%$this->cronInterval==0)
        //         $setting->update([
        //             "ask_from"=>Carbon::now()->second(0),
        //             "ask_at"=>Carbon::now()->addMinutes($this->cronInterval)->second(0),
        //         ]);
        //     else 
        //         $setting->update([
        //             "ask_from"=>Carbon::now()->subMinutes($this->cronInterval)->second(0),
        //             "ask_at"=>Carbon::now()->addMinutes($this->cronInterval)->second(0),
        //         ]);
        // else
        //     $setting->update([
        //         "ask_from"=>Carbon::parse($setting->last_cron_at)->second(0),
        //         "ask_at"=>Carbon::now()->second(0)
        //     ]);

        // //if ASK_AT == LAST_CRON_AT
        // if(Carbon::parse($setting->ask_at)->equalTo(Carbon::parse($setting->last_cron_at)))
        //     $setting->update([
        //         "ask_from"=>Carbon::parse($setting->ask_at)->subMinutes($this->cronInterval)->second(0)
        //     ]);

        // return true;
    }


    /** Send Sms To Numbers 
     *  @var params
    */
    public function processSmsQueueSend($params){
     
        //sms settings
        if(is_null(json_decode($params["setting"]["sms_details"],TRUE))){
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company Settings NULL.");
            return false;
        }

        $smsSettings = json_decode($params["setting"]["sms_details"],TRUE);

        //sms Sender
        if(!isset($smsSettings["sender"])){
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company Sender NULL.");
            return false;
        }

        //sms Text
        if(!isset($params["setting"]["sms_text"])){
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company SMS Text NULL.");
            return false;
        }

        //sms text length
        if(!isset($params["setting"]["sms_text_length"]) || $params["setting"]["sms_text_length"] == 0){
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company SMS Text Length NULL.");
            return false;
        }

        //sms type
        if(!isset($smsSettings["smsType"])){
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company SMS Type NULL.");
            return false;
        }

        //no sms
        if(!isset($smsSettings["noSms"])){
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company noSms NULL.");
            return false;
        }

        //BUILDER
        $autoDialNumbers = NumberAutoDialer::where("company_id",$params["company_id"])->where("has_sms",1);

        if($autoDialNumbers->count()==0) {
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company SMS Numbers EMPTY.");
            return;
        }

        //MESSAGE
        $message = $params["setting"]["sms_text"];

        /** Grabbing Numbers from Autodialer */
        $number4sms = $autoDialNumbers->pluck("number");

        $this->createCronLOG("success",$params["company_id"],"SmsQUEUE: Company SMS Numbers READY. [".count($number4sms)."]");

        dump(">> SMS Numbers.".$number4sms);

        //SENDER
        $sender = $smsSettings["sender"];

        /** If NoSMS is Selected */
        if($smsSettings["noSms"]){

            $remoteCompanyData = $this->intelService->getSmsCompanyByID($params["admin_company_id"]);
            
            if($remoteCompanyData["nosms"]=='')
                $message = $message .= " no ".$sender." 91900";
            else
                $message = $message .= " no ".$remoteCompanyData["nosms"]." 91900";
        }

        /** Sending Via Business SMS via QUEUE 11.26.2021 */
        $businessSms = new \App\Http\Controllers\Backend\SMS\BusinessSmsController(new IntelPhoneService);

        //custom request for Controller
        $request = new Request();
        $request->merge(
            [
                'message'           => $message,
                'messageLength'     => $params["setting"]["sms_text_length"], //requested Since 11.16.2021 caused by EMOJI-s
                'sender'            => $sender,
                'numbers'           => $number4sms, //[0=>"598618319"]
                'company_id'        => $params["company_id"],
                'mor_ip'            => $params["mor_ip"], // for identifying MOR ip
                'assesment'         => true //required for This CRON
            ]);

        //sending request to BusinessSmsController
        $response = $businessSms->send($request);    

        dump(">> SMS Feedback Sent.");

        if(!isset($response["response_Msg"]) || !isset($response["msg"])){
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company SMS response ERROR.");
            return false;
        }

        $resMsg = json_decode($response["response_Msg"],TRUE);

        if($resMsg["status"]!=1){
            $this->createCronLOG("error",$params["company_id"],"SmsQUEUE: Company Sms Delivery FAIL. ".json_encode($response,TRUE));
            return false;
        }

        $this->createCronLOG("success",$params["company_id"],"SmsQUEUE: Company Sms Delivery SUCCESS.");

        /** Blacklist Numbers */
        foreach ($number4sms as $idx => $number)       
            numberBlackListed::firstOrCreate([
                'number'      => $this->numberRemoveGEOIndexPlus($number),
                'company_id'  => $params["company_id"],
                'finishes_at' => Carbon::now()->addHours($params["setting"]["blacklist_int"])
            ]);

        $this->createCronLOG("success",$params["company_id"],"SmsQUEUE: Company Sms Numbers BLACKLIST.");

        //Identifing 
        if($smsSettings["smsType"] == 1)
            foreach ($autoDialNumbers->get() as $idx => $item){

                $voice = NumbersVoiceRecords::where("company_id",$params["company_id"])
                         ->where("number",$item["number"])
                         ->where("has_sms",1);

                $voice_record = $voice->exists() ? $voice->first()["voiceID"] : null;
                $voice_agent = $voice->exists() ? $voice->first()["agent"] : null;

                NumberSmsResponder::firstOrCreate([
                    "company_id"=>$params["company_id"],
                    "number"=>$this->numberRemoveGEOIndexPlus($item["number"]),
                    "agent"=>$voice_agent,
                    "voice_records"=>$voice_record 
                ]);

                if($voice->first())
                   $voice->delete();
            }       

        $this->createCronLOG("success",$params["company_id"],"SmsQUEUE: Company SMS Numbers CLEAR.");

        /** Deleting Previous Autodialer Rows where [has_sms] == 1 */
        $autoDialNumbers->delete();

        return true;
    }

    // returns "u" if contains otherwise [null]
    private function containsGeoKeys($testString) {

        $geoKeys = array('ა','ბ','გ','დ','ე','ვ','ზ','თ','ი','კ','ლ','მ','ნ','ო','პ','ჟ','რ','ს','ტ','უ','ფ','ქ','ღ','ყ','შ','ჩ','ც','ძ','წ','ჭ','ხ','ჯ','ჰ');

        $foundCount = 0;
        str_replace($geoKeys, '', $testString, $foundCount);
        return $foundCount > 0 ? "u" : null;
    }

    /** CRON Part 2 - Every 30 Minutes 
     *  
     *  Autodialer IVR or Sms Poll
    */
    public function processServiceAssesmentPoll(){

        $intervalMin = 30;

        /** Don`t Run CRON if Not any Setting is presented */
        if(count($this->settings) < 1){
            $this->createCronLOG("error",-2,"IVRPoll: Companies OFFLINE.");
            dd("IVR STOP EXECUTE >>");
        }
            
        /** Loop Through Company Settings */
        $this->createCronLOG('success',-2,"IVRPoll: Cron START. ".Carbon::now());

        /** Loop Over Scheduled Companies */
        foreach ($this->settings as $key => $setting) {

            $company = Company::where("id",$setting["company_id"])->first();

            $params = [];
            $params["autodialer_id"] = $setting["autodialer_id"];
            $params["mor_ip"] = $company["mor_ip"];
            $params["campgain_user"] = $company["mordb_user"];

            /** Retrieve Poll Data & Assign VoiceCalls to Numbers */
            $IVRdata = $this->curlRetrieveIVRPollData($setting["company_id"],$params);
            //if ivr_poll_results_string ERROR continue
            if(!$IVRdata) 
                $this->createCronLOG("error",$setting["company_id"],"IVRPoll: cURL Data NULL.");         
            
        };

        $this->createCronLOG("success",-2,"IVRPoll: Cron FINISH. ".Carbon::now());         

        dd("IVR Data FINISH.");
    }



    /** DISABLED FUNCTIONS 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
    */


    /**
     * 
     *      Restart Campaign
     *      @var id - 17 Our Autodialer ID
     *      @var company_id - for MOR IP
     */
    protected function restartCampaignStatus($id,$company_id){
        //stop
        $status = $this->changeCampaignStatus($id,$company_id);
        //start
        $status = $this->changeCampaignStatus($id,$company_id);
        //dd($status." ".Carbon::now());
        //temporary disable
        $this->createCronLOG('success',-1,"Campgain ID [".$id."] RESTART. ".Carbon::now());
        return $status;
    }

    /**
     * 
     *     Change Campaign Status
     *     @var id - Campaign ID
     *     @var company_id - for MOR IP
     */
    protected function changeCampaignStatus($id,$company_id){

        $company = DB::table('companies')
        ->where('id', $company_id)
        ->first();

        // pass credentials
        $campgain_user     = "2492020";
        $secretKey         = 'case123';
        // autodialer id
        $campgain_id       = $id;
        $hash       = sha1($campgain_id.$secretKey);
        $url        = config('mor.autodialer_change_campaign_status.'.$company->mor_ip)."?u={$campgain_user}&id={$campgain_id}&hash={$hash}";

        $post = array('u' => $campgain_user,'id' => $campgain_id);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FAILONERROR, true); 

        //process Curl
        $url        = str_replace(" ", "%20", $url);
        $xml2json   = json_encode(simplexml_load_string(file_get_contents($url)), true);
        $loadJson   = json_decode($xml2json, true);

        // 09.11.20 INEXPHONE Autodialer Sergi`s Version ID was 17
        if(isset($loadJson["status"]["error"]))
            // Fix Not Date Update at ASK_FROM!!!
            return $loadJson["status"]["error"];
        return $loadJson["status"]["success"];
    }


    /** 
     * 
     *  @var morIP
     *  @var queryParams
    */
    protected function getMorQueueLogByParams($queryParams,$morIP){

        $url            = config('mor.queue_log_get.'.$morIP)."?".http_build_query($queryParams);
    
        $result = $this->curlPost($url, [
            'u'         => $queryParams["u"],
            'hash'      => $queryParams["hash"],
            'from'      => $queryParams["from"],
            'till'      => $queryParams["till"],
            'queuename' => $queryParams["queuename"],
            'event'     => $queryParams["event"]
        ]);

        $xml2json   = json_encode(simplexml_load_string($result), true); // convert xml data to json

        $loadJson   = json_decode($xml2json, true);

        // Retrieve Calls
        return collect($loadJson)->flatten(2);

    }


    /**
     *    Parse Numbers After MOR ask
     *    @var json - json Array after MOR Request
     *    @var companyID - authorised Company ID
     */
    protected function processAfterMOR($json,$companyID){

        /**
         *   GOAL
         *   get array of  [ Phone Numbers ] => [ [ Voice Record ID 1 ] , [ Voice Record ID 2 ].... ]
         */

        $numbersWithCallIDs = [];

        foreach ($json as $idx => $queueEntrie)
            if($queueEntrie["event"]=="ENTERQUEUE")
                $numbersWithCallIDs[$queueEntrie["data2"]]["call_ids"][] = $queueEntrie["call_id"];
        

        /**
         *   GOAL
         *   get array of  [ Phone Numbers ] => [ [ Voice Record ID 1 ] , [ Voice Record ID 2 ].... ]
         */

        /** Get The Numbers Received From MOR. */
        $numbersWithCallIDs = [];
        /** Call IDs with Voice Records */
        $callIdsWithVoiceRecords=[];
        /** Number with Voice Records */
        $numbersWithVoiceRecords = [];

        $sassSetting = MORaskSettings::where("company_id",$companyID)->first();

        /**
         *    Assotiative Array
         *    Example::
         * 
         *    [ 322070397 ] array: 2 [
         *     
         *     // CALL IDs             // DATES
         *     "1598509583.581388" => "2020-08-27 10:26:45"
         *     "1598509670.581963" => "2020-08-27 10:28:11"
         *    ] 
         */

        foreach ($json as $key => $value) {
            /** abort if Error - Commented At 09.21.20 */
            //if(isset($value['event'])) return false;

            if($value['event'] == 'ENTERQUEUE'){ //'ABANDONED'
                // adjust number Remove +995 if EXISTS
                $value['data2'] = $this->numberRemoveGEOIndexPlus($value['data2']);

                // don`t take number if it`s BlackListed or Excluded & Save For AutoDialer
                if($this->isNumberExcluded($value['data2'],$companyID) || $this->isNumberBlackListed($value['data2'],$companyID)){}
                else 
                    $numbersWithCallIDs[$value['data2']][$value['call_id']] = $value['time'];


                // // don`t take number if it`s BlackListed or Excluded & Save For AutoDialer
                // if(!$this->isNumberExcluded($value['data2'],$companyID) && !$this->isNumberBlackListed($value['data2'],$companyID)){

                //     $numbersWithCallIDs[$value['data2']][$value['call_id']] = $value['time'];
                // }
            }

            /** Remove EVENT == ABANDON numbers IF EXIST */
            if($value['event'] == 'ABANDON'){
                foreach ($numbersWithCallIDs as $number => $callID) {
                    if(key($callID) == $value['call_id']){
                        unset($numbersWithCallIDs[$number]);
                    }
                    else{
                        /** Save Numbers For AutoDialer [prevent duplicates] */
                        NumberAutoDialer::firstOrCreate([
                            'number' => $this->numberRemoveGEOIndexPlus($number),
                            'company_id' => $companyID,
                            'has_sms'=> $sassSetting["has_sms"] == 1 ? 1 : 0
                        ]); 
                    } 
                }
            }
        }


         /**
         *     // CALL IDs             // VOICE RECORD IDs
         *    "1598509583.581388" => "1598509583.581386"
         * 
         */

        /**
         *    New Version 10.13.20
         * 
         *       CALL IDs                   
         *    "1598509583.581388" => array( 
         *                                     VOICE RECORD IDs       MOR Extension
         *                                  "1598509583.581386" => "Local/602@pool_24_mor_local" 
         *                                  )
         * 
         */

        foreach ($json as $key => $value) {
            if($value['event'] == 'CONNECT')//data4 - voice record id
                {
                    $callIdsWithVoiceRecords[$value['call_id']][$value['data4']] = $value['agent'];
                    // OLD Version $callIdsWithVoiceRecords[$value['call_id']] = $value['data4'];
                }
        }

        /**
         *      PHONE NUMBERs          VOICE RECORD IDs
         *      322070397       =>    "1598509669.581961"
         */

        /**
         *      New Version 10.13.20
         *      
         *   555406555 => array:3 [▼
         *          "1602577231.359521" => array:1 [▼
         *          "1602577231.359521" => "Local/602@pool_24_mor_local"
         *       ]
         * 
         */

        foreach ($numbersWithCallIDs as $number => $callid_date) {
            foreach ($callid_date as $callID => $date) {
                if(array_key_exists($callID,$callIdsWithVoiceRecords)){
                    // OLD
                    //$numbersWithVoiceRecords[$number] = $callIdsWithVoiceRecords[$callID];
                    // NEW
                    $numbersWithVoiceRecords[$number][key($callIdsWithVoiceRecords[$callID])] = $callIdsWithVoiceRecords[$callID];
                }
            }
        }

        /** Save in database for current company - numbers which HAD voice Records */
        foreach ($numbersWithVoiceRecords as $number => $voiceID) {
            //dump(key($voiceID));
            //dump(array_values($voiceID[key($voiceID)])[0]);

            // Save Unique Voice ID
            $record = NumbersVoiceRecords::where('voiceID',$voiceID)->first();
            if(!$record){
                /** Save Voice Records */
                NumbersVoiceRecords::Create([
                    'company_id'    => $companyID,
                    'number'        => $this->numberRemoveGEOIndexPlus($number),
                    'voiceID'       => key($voiceID)
                ]);

                /** OLD */
                // NumbersVoiceRecords::Create([
                //     'company_id'    => $companyID,
                //     'number'        => $number,
                //     'voiceID'       => $voiceID
                // ]);

                /** [number] => [ call id ] => array ([ call id ] => [ Local/602@pool_24_mor_local ]) **/

                $agentFullText = array_values($voiceID[key($voiceID)])[0]; // "Local/602@pool_24_mor_local"
                $agentID   = substr($agentFullText,strpos($agentFullText,"/")+1,strpos($agentFullText,"@")-(strpos($agentFullText,"/")+1)); // 602

                /** Save Numbers For AutoDialer [prevent duplicates] */
                NumberAutoDialer::firstOrCreate([
                    'number'     => $this->numberRemoveGEOIndexPlus($number),
                    'company_id' => $companyID,
                    'agent'      => $agentID,
                    'has_sms'=> $sassSetting["has_sms"] == 1 ? 1 : 0
                ]);  
            }
        }
        return true;
    }

    /** Update Last Cron AT Time */
    protected function updateLastCronAT($setting){

        return true;
        //will be removed

        $minuteNow = Carbon::now()->second(0)->format("i");
        $lastCron = $minuteNow-$minuteNow%$this->cronInterval;
        // 16 -> 16 - 16%5 = 15

        $setting->update([
                "last_cron_at"=>Carbon::now()->minute($lastCron)->second(0),
                "ask_from"=>Carbon::now()->minute($lastCron)->second(0),
                "ask_at"=>Carbon::now()->minute($lastCron)->addMinutes($this->cronInterval)->second(0)
            ]);
        return true;
    }

    /** 
     *  CDR Channel
     *  @var params
     *  @var switch - TRUE by default INCOMMING
     */
    protected function getCompanyCDR($params,$switch=true){

        //$this->createCronLOG("success",$params['company_id'],"Company CDR START.");

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        ini_set("memory_limit", "-1");
        set_time_limit(0);

        //needsSmsEvaluation
        $has_sms = json_decode($params['setting'],TRUE)["has_sms"]==1?1:0;

        $query["period_start"]      = $params["base_from_stamp"];
        $query["period_end"]        = $params["base_till_stamp"];
        $query["s_user"]            = $params["s_user"];
        $query["s_call_type"]       = 'answered';
        
        $hash           = sha1(implode("",array_values($query))."case123");
        $query["u"]     = config('mor.query.u');
        $query["hash"]  = $hash;

        $url            = config('mor.user_calls_get.'.$params["mor_ip"])."?".http_build_query($query);

        $result = $this->curlPost($url, [
            'u'     => $query["u"],
            'hash'  => $query["hash"]
        ]);

        $xml2json   = json_encode(simplexml_load_string($result), true); // convert xml data to json
        $data       = json_decode($xml2json, true)['calls_stat'];

        //dispod "ANSWERED"
        $answereds = [];

        $this->createCronLOG("success",$params['company_id'],"CDR: Data RECEIVING.");

        if( !empty($data['calls']) && !empty($data['calls']['call'])) {

            $this->createCronLOG("success",$params['company_id'],"CDR: Data Receive DONE.");

            $calls = $data['calls']['call'];

            //fix No DATA
            if(!isset($calls["0"])){
                $temp = $calls;
                $calls = [];
                $calls[0] = $temp;
            }

            if($switch)
                $answereds =  array_filter($calls, function($item) use($switch){
                    if($item['dst_user_id'] == 0) return false; //INCOMMING CALLS QUEUE
                        return $item;
                });
            else
                $answereds =  array_filter($calls, function($item) use($switch){
                    if($item['dst_user_id'] != 0) return false; //OUTGOING DEVICES
                        return $item;
                });

            $companyID = $params['company_id'];

            //getting all DEVICE Data
            if(!$switch){

                $this->createCronLOG("success",$params['company_id'],"CDR: Device Data PARSE.");

                array_filter($answereds,function($item) use($companyID,$has_sms,$answereds){
                    //getting number       
                    $number = $this->numberRemoveGEOIndexPlus($item["dst"]);

                    //getting full extension text
                    $extName = substr($item["clid"],0,strpos($item["clid"]," <"));

                    //database extension
                    $dbExt = MorExtension::where("extension","like","%".$extName."%")
                                             ->orWhere("callerid","like","%".$extName."%")->first();

                    //don`t send To Voice Records if Excluded Or BlackListed
                    if($this->isNumberExcluded($number,$companyID) || $this->isNumberBlackListed($number,$companyID)){}
                    else{
                        
                        $numberData = NumbersVoiceRecords::where('number',$number)->where('company_id',$companyID)->where("cdr",1)->first();

                        //saving AGENT data in Voice Records
                        if(!$numberData){

                            NumbersVoiceRecords::create([
                                'company_id'    => $companyID,
                                'number'        => $number,
                                'has_sms'       => $has_sms,
                                'agent'         => is_null($dbExt["extension"])?null:$dbExt["extension"],
                                'voiceID'       => null,
                                'call_date'     => $item["calldate2"],
                                'answer_date'   => $item["calldate2"],
                                'cdr'           => 1
                            ]);

                            NumberAutoDialer::firstOrCreate([
                                'number'        => $number,
                                'company_id'    => $companyID,
                                'has_sms'       => $has_sms,
                                'sent'          =>0
                            ]);
                        }

                    }

                    return true;//go to next array value
                });

                $this->createCronLOG("success",$companyID,"CDR: Company Device Numbers SAVE.");

            }

            else{//getting all QUEUE Data

                $this->createCronLOG("success",$params['company_id'],"CDR: Queue START.");

                //grabbing numbers
                $numbers = array_map(function($item){
                    return $this->numberRemoveGEOIndexPlus($item["src"]);
                },$answereds);

                //get unique numbers
                $numbers = array_values(array_unique($numbers));

                $this->createCronLOG("success",$params['company_id'],"CDR: Company Queue Numbers SAVE.");

                //Preparing numbers for Autodialer
                foreach ($numbers as $idx => $number) {
                    //don`t send if Exluded or BlackListed
                    if($this->isNumberExcluded($number,$params['company_id']) || $this->isNumberBlackListed($number,$params['company_id'])){}
                    else
                        if(!NumberAutoDialer::where('number',$number)->where('company_id',$params['company_id'])->first())
                            NumberAutoDialer::create([
                                'number'        => $number,
                                'company_id'    => $params['company_id'],
                                'has_sms'       => $has_sms,
                                'sent'          =>0
                            ]);
                }

            }    

        }
        else 
            $this->createCronLOG("success",$params['company_id'],"CDR: Data EMPTY.");
        return true;
    }
}
